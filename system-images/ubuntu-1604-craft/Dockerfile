FROM ubuntu:16.04

LABEL Description="KDE Ubuntu 16.04 Base for use with Craft"
MAINTAINER KDE Sysadmin <sysadmin@kde.org>

# Start off as root
USER root

# Make sure we are fully up to date to start with and install our core operational requirements
RUN apt-get update && apt-get install -y software-properties-common python-software-properties && add-apt-repository -y ppa:openjdk-r/ppa
RUN apt-get update && apt-get upgrade -y && apt-get install -y openssh-server openjdk-8-jre-headless

# Craft and Craftmaster need Python 3.6 at a minimum
RUN add-apt-repository ppa:deadsnakes/ppa && apt-get update && apt-get install -y python3.6

# The CI Tooling requires YAML and XML support in Python, along with Paramiko
RUN apt-get install -y python3-yaml python3-lxml python3-paramiko

# Enable the source repositories
RUN cat /etc/apt/sources.list | sed -e 's,# deb-src,deb-src,' > /etc/apt/sources.list.new && mv /etc/apt/sources.list.new /etc/apt/sources.list && apt-get update

# Now install the general dependencies we need for builds
RUN apt-get install -y \
  # General requirements for building KDE software
  build-essential cmake git-core \
  # General requirements for building other software
  automake ruby nasm libxml-perl libxml-parser-perl uuid-dev \
  # Needed to make sure we have working locales
  locales \
  # Needed for some frameworks
  bison gettext \
  # Qt and KDE Build Dependencies
  gperf libasound2-dev libatkmm-1.6-dev libbz2-dev libcairo-perl libcap-dev libcups2-dev libdbus-1-dev \
  libdrm-dev libegl1-mesa-dev libfontconfig1-dev libfreetype6-dev libgcrypt11-dev libgl1-mesa-dev \
  libglib-perl libgsl0-dev libgstreamer-plugins-base0.10-dev libgstreamer0.10-dev \
  libgtk2-perl libjpeg-dev libnss3-dev libpci-dev libpng12-dev libpulse-dev libssl-dev \
  libtiff5-dev libudev-dev libwebp-dev flex \
  # Mesa libraries for everything to use
  libx11-dev libxcb-glx0-dev libxcb-keysyms1-dev libxcb-util0-dev libxcb1-dev libxcb-res0-dev libxcomposite-dev libxcursor-dev \
  libxdamage-dev libxext-dev libxfixes-dev libxi-dev libxrandr-dev libxrender-dev libxss-dev libxtst-dev mesa-common-dev \
  libxcb-xinerama0-dev

# Because we are going to build Qt definitely make sure all of it's dependencies get dragged in
RUN apt-get build-dep -y qt5-default

# Setup a user account for everything else to be done under
RUN useradd -d /home/appimage/ -u 1000 --user-group --create-home -G video appimage
# Make sure SSHD will be able to startup
RUN mkdir /var/run/sshd/
# Get locales in order
RUN locale-gen en_US en_US.UTF-8 en_NZ.UTF-8

# We want to run SSHD so that Jenkins can remotely connect to this container
EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]
